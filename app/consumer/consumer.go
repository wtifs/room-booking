package consumer

import (
	"context"
	"encoding/json"
	"gitee.com/wtifs/room-booking/app/model"
	"gitee.com/wtifs/room-booking/app/service/booking"
	"gitee.com/wtifs/room-booking/app/service/log"
	"gitee.com/wtifs/room-booking/app/service/recovery"
	"github.com/Shopify/sarama"
	"github.com/wvanbergen/kafka/consumergroup"
	"golang.org/x/sync/semaphore"
	"sync/atomic"
	"time"
)

const (
	BOOKING_STATUS_CG    = "momo-q1c28"
	BOOKING_STATUS_TOPIC = "meeting_status_channel"
)

var (
	zks = []string{
		"10.30.102.60:2181",
	}

	PREDICTS = map[string]string{
		"free":   "busy",
		"busy":   "free",
		"closed": "free",
	}

	success, fail int32
)

type roomBookingKafkaConsumer struct {
	consumerGroup *consumergroup.ConsumerGroup
	sema          *semaphore.Weighted
}

func (consumer *roomBookingKafkaConsumer) init() error {
	consumer.sema = semaphore.NewWeighted(3)
	cfg := consumergroup.NewConfig()
	cfg.Offsets.Initial = sarama.OffsetNewest
	cfg.Offsets.ProcessingTimeout = 10 * time.Second

	var err error
	consumer.consumerGroup, err = consumergroup.JoinConsumerGroup(BOOKING_STATUS_CG, []string{BOOKING_STATUS_TOPIC}, zks, cfg)
	if err != nil {
		return err
	} else {
		log.Info("%s: join consumer group %s successfully", BOOKING_STATUS_TOPIC, BOOKING_STATUS_CG)
	}
	go func() {
		for err := range consumer.consumerGroup.Errors() {
			log.Err("consumer group error: %s", err.Error())
		}
	}()
	return nil
}

//for循环消费
func (consumer *roomBookingKafkaConsumer) consume(ctx context.Context) {
ConsumeMessage:
	for {
		select {
		case <-ctx.Done():
			if err := consumer.consumerGroup.Close(); err != nil {
				log.Err("marketing_user_activation: error closing the consumer: %s", err.Error())
			}
			break ConsumeMessage
		case msg := <-consumer.consumerGroup.Messages():

			go consumer.ProcessMsg(ctx, msg.Value)

			//commit after process, confirm at least once
			err := consumer.consumerGroup.CommitUpto(msg)
			if err != nil {
				log.Err("%s: error committing: %s", BOOKING_STATUS_TOPIC, err.Error())
			}
		}
	}
}

func (c *roomBookingKafkaConsumer) ProcessMsg(ctx context.Context, msg []byte) {
	topic := BOOKING_STATUS_TOPIC
	log.Info("%s: %s", topic, msg)

	room := model.RoomStatusChangeMsg{}
	_ = json.Unmarshal(msg, &room)

	roomID := room.MeetingRoomID

	// 预测下个状态
	predictedStatus := PREDICTS[booking.Rooms[roomID].Status]
	booking.AvailableRooms.Lock()
	booking.AvailableRooms.M[roomID] = predictedStatus != "closed"
	booking.AvailableRooms.Unlock()

	booking.Rooms[roomID].Lock()
	booking.Rooms[roomID].Status = predictedStatus
	booking.Rooms[roomID].Unlock()
	//booking.StatusChangedRooms <- roomID

	status, err := booking.GetRoomInfo(roomID)
	if err != nil {
		log.Err(err.Error())
		return
	}

	// 如果和预测状态一样，则不用再发了
	if status == "请求太频繁" {
		return
	}

	booking.AvailableRooms.Lock()
	booking.AvailableRooms.M[roomID] = status != "closed"
	booking.AvailableRooms.Unlock()

	if status == predictedStatus {
		atomic.AddInt32(&success, 1)
	} else {
		atomic.AddInt32(&fail, 1)
	}

	log.Warning("%d: predicted status: %s. actual status: %s. success: %d. fail: %d", roomID, predictedStatus, status, success, fail)

	booking.Rooms[roomID].Lock()
	booking.Rooms[roomID].Status = status
	booking.Rooms[roomID].Unlock()
	//booking.StatusChangedRooms <- roomID
}

func RunBookingStatusConsumer() {
	defer recovery.Recovery("run consumer")

	kafkaConsumer := &roomBookingKafkaConsumer{}

	ctx := context.Background()
	err := kafkaConsumer.init()
	if err != nil {
		log.Err("consumer init error: %s", err.Error())
		return
	}
	kafkaConsumer.consume(ctx)
}
