package model

import (
	"net/http"
	"sync"
)

type RoomClient struct {
	sync.Mutex
	Client http.Client
	Status string
	Cancel func()
}
