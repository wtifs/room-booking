package model

import "sync"

type AvailableRooms struct {
	M map[int]bool
	sync.Mutex
}
