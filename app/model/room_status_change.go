package model

type RoomStatusChangeMsg struct {
	MeetingRoomID int `json:"meeting_room_id"`
}
