package booking

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/wtifs/room-booking/app/model"
	"gitee.com/wtifs/room-booking/app/service/log"
	"github.com/pkg/errors"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

var (
	machines           = []string{"10.30.102.147", "10.30.102.148", "10.30.102.149"}
	Rooms              []model.RoomClient
	StatusChangedRooms = make(chan int, 3)
	AvailableRooms     = model.AvailableRooms{
		M: make(map[int]bool, 31),
	}
)

func init() {
	Rooms = make([]model.RoomClient, 31)

	for i := 1; i <= 30; i++ {
		Rooms[i] = model.RoomClient{
			Status: "free",
		}
		AvailableRooms.M[i] = true
	}
}

func randMachine() string {
	return machines[rand.Intn(3)]
}

func GetRoomInfo(roomID int) (string, error) {
	values := map[string]string{"user_token": "6001ff8445378791b8d8f1524660c983f0dc70c8", "meeting_room_id": strconv.Itoa(roomID)}
	jsonData, err := json.Marshal(values)
	if err != nil {
		return "", errors.Wrap(err, "marshal")
	}
	resp, err := http.Post(fmt.Sprintf("http://%s/api/info", randMachine()), "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return "", errors.Wrap(err, "post")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.Wrap(err, "read body")
	}

	log.Info("room status: %d: %s", roomID, body)
	res := strings.ReplaceAll(string(body), `"`, "")
	return res, nil
}

func Book(roomId int) int {

	values := map[string]string{"user_token": "6001ff8445378791b8d8f1524660c983f0dc70c8", "meeting_room_id": strconv.Itoa(roomId)}
	jsonData, err := json.Marshal(values)
	if err != nil {
		log.Println(err)
		return 400
	}

	ctx, cancel := context.WithCancel(context.Background())
	Rooms[roomId].Cancel = cancel

	req, err := http.NewRequestWithContext(ctx, "POST", fmt.Sprintf("http://%s/api/book", randMachine()), bytes.NewBuffer(jsonData))
	if err != nil {
		log.Err("构造 http 请求错误 %+v", err)
		return 400
	}
	//client := http.Client{}
	//resp, err := client.Do(req)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Err("do http 请求错误 %+v", err)
		resp, err := http.Post(fmt.Sprintf("http://%s/api/book", randMachine()), "application/json", bytes.NewBuffer(jsonData))
		if err != nil {
			log.Err("-----***** 预定房间请求问题 ***** %+v", err)
			return 401
		}
		if resp.Body != nil {
			defer resp.Body.Close()
			return 400
		}
	}

	if resp.Body != nil {
		defer resp.Body.Close()
		return 400
	}
	code := resp.StatusCode
	return code
}
