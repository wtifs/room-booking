package main

import (
	"gitee.com/wtifs/room-booking/app/consumer"
	"gitee.com/wtifs/room-booking/app/model"
	"gitee.com/wtifs/room-booking/app/service/booking"
	"gitee.com/wtifs/room-booking/app/service/log"
	"strconv"
	"sync/atomic"
	"time"
)

var BookNum int32 = 0

func main() {
	log.Info("===== start =====")

	go consumer.RunBookingStatusConsumer()
	go bookOne()
	//go dealKafkaChan()

	ch := make(chan bool)
	ch <- true

	log.Info("===== bye =====")
}

func bookOne() {
	ticker := time.NewTicker(time.Millisecond * 100)
	i, j := 25, 30

	for range ticker.C {
		booking.AvailableRooms.Lock()
		if booking.AvailableRooms.M[i] {
			go booking.Book(i)
		} else {
			i = i%30 + 1
		}

		if booking.AvailableRooms.M[j] {
			go booking.Book(j)
		} else {
			j = j%30 + 1
		}

		booking.AvailableRooms.Unlock()
	}
}

func dealKafkaChan() {
	for id := range booking.StatusChangedRooms {
		go dealRoomClient(id)
	}
}

func dealRoomClient(id int) {
	if id == 0 || id > 30 {
		log.Err("拿到了比 30 大的房间号 %+v", id)
		return
	}
	roomClient := &booking.Rooms[id]
	roomClient.Lock()
	defer roomClient.Unlock()
	log.Info("----- 取到的 room : %+v 的状态 %+v", id, roomClient.Status)
	if roomClient.Status == "busy" {
		//  free 直接请求
		if atomic.LoadInt32(&BookNum) < 3 && atomic.LoadInt32(&BookNum) >= 0 {
			atomic.AddInt32(&BookNum, 1)
		}
		log.Info("开始对 %+v 进行 book", id)
		book(strconv.Itoa(id), roomClient)
	} else {
		log.Info("----不是 free 的状态，打印出来 %+v", roomClient.Status)
		if roomClient.Cancel != nil { // 如果这个客户端正在跑 book，但是我们检测到了已经被占用，就直接取消, 并释放一个令牌
			roomClient.Cancel()
			log.Info("已经完成 cancel %+v", id)
			if atomic.LoadInt32(&BookNum) <= 3 && atomic.LoadInt32(&BookNum) > 0 {
				atomic.AddInt32(&BookNum, -1)
			}
			roomClient.Cancel = nil
		}
	}
}

func book(id string, client *model.RoomClient) {
	idInt, _ := strconv.Atoi(id)
	code := booking.Book(idInt)
	log.Info("*******   post book 到房间的状态是 id:%+v, code:%+v ********", id, code)
	defer atomic.AddInt32(&BookNum, -1)
	// 获得了请求结果后，把 cancel 置为空，防止误判
	client.Cancel = nil
	if code == 200 {
		log.Info("******************* 订上房间啦 ****************")
		client.Status = "busy"
	} else if code == 401 {
		log.Info("************** 房间被 cancel 取消啦 ***************")
		client.Status = "busy"
	} else {
		log.Info("************** 房间被定啦 ***************")
		client.Status = "busy"
	}
}
